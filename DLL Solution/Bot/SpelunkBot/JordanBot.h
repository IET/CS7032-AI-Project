#pragma once
#include "IBot.h"
#include <iostream>
#include <fstream>

class JordanBot : public IBot
{
private:
	bool	_canFall;
	bool	_canGoRight;
	bool	_canGoLeft;
	bool	_canGoForward;
	bool	_canJumpRight;
	bool	_canJumpLeft;
	bool	_canJumpForward;
	bool	_canJumpGrabRight;
	bool	_canJumpGrabLeft;
	bool	_canJumpGrabForward;
	bool	_isHanging;
	bool	_isAtLadder;

	bool	_onEdgeBeforeWall;

	bool	_wantToGoUp;
	bool	_wantToGoDown;
	bool	_wantToGoLeft;
	bool	_wantToGoRight;

	bool	_goingUpLadder;
	bool	_goingDownLadder;
	
	int		_facing;

	void TurnAround();

	void UpdateBools();

	void PrintPlayerVariables();

	//Performance Metrics
	int		_levelsCompleted;

	void	WriteOutData();

	double GetNodeStateCurrent();
	double GetNodeStateUp(int num);
	double GetNodeStateDown(int num);
	double GetNodeStateLeft(int num);
	double GetNodeStateRight(int num);
	double GetNodeStateForward(int num);

	std::ofstream performanceOutput;
	std::ifstream performanceInput;
public:
	JordanBot();
	virtual ~JordanBot() { }

	void NewLevel() override;
	void Update() override;
};

