#pragma once
#include "IBot.h"
#include <iostream>
#include <fstream>

enum actions  { MOVE_LEFT, MOVE_RIGHT, JUMP_LEFT, JUMP_RIGHT };

const int NUM_ACTIONS =	4;

const int NUM_NODES_X = 3;
const int NUM_NODES_Y = 3;

const int STATES_PER_NODE = 2;  //Initially just empty/not empty

//TODO - Implement this in a less static way. Ideally, would probably be a pointer instead.
//NUM_STATES = 2^((NUM_NODES_X * NUM_NODES_Y) - 1)
const int NUM_STATES = 256; 

//Both ranging from 0 to 1. 
const float learningConstant = 0.15f;
const float	discountFactor = 0.8f;

//Greed factor
const float epsilon = 0.7f;

class LearnBot : public IBot
{
private:
	bool	_canFall;
	bool	_canGoRight;
	bool	_canGoLeft;
	bool	_canGoForward;
	bool	_canJumpRight;
	bool	_canJumpLeft;
	bool	_canJumpForward;
	bool	_canJumpGrabRight;
	bool	_canJumpGrabLeft;
	bool	_canJumpGrabForward;
	bool	_isHanging;
	bool	_isAtLadder;
	bool	_firstUpdate;

	int		_reward;

	//Learning Variables
	int		currentState;
	int		previousState;
	int		previousAction;
	int		maxDepth;


	//Value of each action in each state
	float	V[NUM_STATES][NUM_ACTIONS];
	
	int		GetMaxValue(int state);
		
	
	//Functions for learning algorithm
	//Greedily choose the best action
	int	ChooseOptimalAction();
	
	//Follow epsilon policy to decide whether to exploit or explore
	int	EpsilonPolicy();
	
	//Returns the reward for the current state
	float CalculateReward();

	//Find bot's current state
	void DetermineCurrentState();

	//Update state-action values based on Sarsa algorithm
	void SarsaUpdateV();

	//Convenience function for getting state of current node (Should probably be defined in base class)
	double GetNodeStateCurrent();

	//Streams to read/write value data
	std::ifstream valueInput;
	std::ofstream valueOutput;

	//Write out relevant data
	void	WriteOutData();
public:
	LearnBot();
	virtual ~LearnBot() {};

	void NewLevel() override;
	void Update() override;
};

