#include "stdafx.h"
#include "LearnBot.h"
#include <vector>
#include <stdlib.h>     // srand, rand
#include <time.h>       // current time for random seed
#include <string>		//string, stof

void LearnBot::NewLevel()
{
	InitialiseVariables();
}

void LearnBot::Update() {

	if (!_firstUpdate)
	{
		previousState = currentState;
		currentState = 0;
	}
	else {
		//If first run, initialise
		maxDepth = (int)_playerPositionYNode;
		currentState = 0;
		previousState = 0;
		previousAction = 0;
		_firstUpdate = false;
	}

	DetermineCurrentState();

	//Update Value of Actions Using Sarsa
	//Based on maximum action value of current state
	SarsaUpdateV();

	//If at the exit, leave
	if (GetNodeStateCurrent() == spExit) {
		_lookUp = true;
		//Store array of value data so that it is remembered for subsequent runs
		WriteOutData();
		return;
	}

	//Choose an action
	switch (previousAction = EpsilonPolicy()) {
	case actions::MOVE_LEFT:
		std::cout << "move left chosen" << std::endl;
		_goLeft = true;
		break;
	case actions::MOVE_RIGHT:
		std::cout << "move right chosen" << std::endl;
		_goRight = true;
		break;
	case actions::JUMP_LEFT:
		std::cout << "jump left chosen" << std::endl;
		_jump = true;
		_goLeft = true;
		break;
	case actions::JUMP_RIGHT:
		std::cout << "jump right chosen" << std::endl;
		_jump = true;
		_goRight = true;
		break;
	default:
		break;
	}
}

int LearnBot::EpsilonPolicy() {
	int choice;

	srand(time(NULL));

	if ( ((rand() % 10) / 10.0f) < epsilon) {
		//Be greedy, choose best action
		return ChooseOptimalAction();
	}
	else {
		//Select randomly, explore
		return rand() % NUM_ACTIONS;
	}
	
	return 0;
}

int LearnBot::ChooseOptimalAction() {
	std::vector<int> optimalActions;
	float max_val = 0.0f;

	for (int i = 0; i < NUM_ACTIONS; i++) 
	{
		if (V[currentState][i] > max_val)
			max_val = V[currentState][i];
	}

	//check for multiple actions at max probability
	for (int i = 0; i < NUM_ACTIONS; i++)
	{
		if (max_val == V[currentState][i])
			optimalActions.push_back(i);
	}

	//If there are no ties, pick best action
	if (optimalActions.size() == 1)
	{
		return optimalActions[0];
	}
	else 
	{
		//Otherwise select action randomly from optimal actions determined above
		srand(time(NULL) * _frameNumber);
		int choice = optimalActions[rand() % optimalActions.size()];
		return choice;

	}
}

LearnBot::LearnBot() {
	std::cout << "Constructor Called";
	_reward = 0;

	valueInput.open("ValueData.txt");
	//If file exists (bot has been run before)
	if (valueInput.good()) {
		std::cout << "Reading value data from file";
		for (int i = 0; i < NUM_STATES; i++) {
			for (int j = 0; j < NUM_ACTIONS; j++) {
				std::string line;
				getline(valueInput, line);
				V[i][j] = std::stof(line);
			}
		}
	}

	valueInput.close();

	for (int i = 0; i < NUM_STATES; i++) {
		for (int j = 0; j < NUM_ACTIONS; j++) {
			std::cout << "Initialising Value For State: " << i << "Action: " << j << std::endl;
			V[i][j] = 1.0f;
		}
	}
}

void LearnBot::WriteOutData()
{
	valueOutput.open("ValueData.txt");
	
	for (int i = 0; i < NUM_STATES; i++) {
		for (int j = 0; j < NUM_ACTIONS; j++) {
			valueOutput << V[i][j] << std::endl;
		}
	}

	valueOutput.close();
}

void LearnBot::DetermineCurrentState() {
	//Extremely Naieve Method for Determining State
	std::cout << "Determining Current State..." << std::endl;
	for (int i = 0; i < (NUM_NODES_X * NUM_NODES_Y); i++) {
		if (i == ((NUM_NODES_X * NUM_NODES_Y) / 2))
			continue;	//Skip player position
		currentState |= (int)IsNodePassable((_playerPositionXNode - NUM_NODES_X / 2) + (i % NUM_NODES_X), (_playerPositionYNode - NUM_NODES_Y / 2) + (i / NUM_NODES_X), NODE_COORDS) << i;
	}
	//Just shifting everything over to allow for the fact that the player node was skipped so we only have 8 nodes in 9 bits
	currentState |= ((currentState & 0x1e0) >> 1);
	currentState &= 0xFF;
}

//Find the best value available in this state
int LearnBot::GetMaxValue(int state) {
	float max_val = 0.0f;

	for (int i = 0; i < NUM_ACTIONS; i++) {
		if (V[state][i] > max_val)
			max_val = V[state][i];
	}
	
	return max_val;
}

float LearnBot::CalculateReward() {
	float reward = 0.0f;

	if ((int)_playerPositionYNode > maxDepth) 
	{
		//Could be better to scale by how much depth was gained
		//int depth_gained = _playerPositionYNode - maxDepth;
		//return depth_gained / 5.0f;
		
		//std::cout << "Player Y Node: " << _playerPositionYNode <<std::endl;
		//std::cout << "Previous Max: " << maxDepth <<std::endl;

		maxDepth = _playerPositionYNode;
		reward += (1.0f); //Just assign reward for gaining depth
	}

	if (GetNodeStateCurrent() == spExit)
		reward += 2.0f;

	return reward;
}

//Value update using Sarsa
void LearnBot::SarsaUpdateV() {
	float reward = CalculateReward();

	std::cout << "Reward: " << reward << std::endl;
	std::cout << "Added Value: " << learningConstant * ((reward + (discountFactor * GetMaxValue(currentState)) - V[previousState][previousAction])) <<std::endl;

	V[previousState][previousAction] += learningConstant * ((reward + (discountFactor * GetMaxValue(currentState)) - V[previousState][previousAction] ));
}

double LearnBot::GetNodeStateCurrent()
{
	return GetNodeState(_playerPositionXNode, _playerPositionYNode, NODE_COORDS);
}