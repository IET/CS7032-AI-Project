#include "stdafx.h"
#include "JordanBot.h"


void JordanBot::NewLevel()
{
	InitialiseVariables();
}

void JordanBot::Update()
{
	UpdateBools();

	//Not sure if this is the best place for this but it seemed like the quickest way to test this.
	if (_firstUpdate) {
		_targetY = 470;
		_targetX = 300;
		CalculatePathFromXYtoXY(_playerPositionX, _playerPositionY, _targetX, _targetY, PIXEL_COORDS);

		_firstUpdate = false;
	}

	WriteOutData();

	// Search for the exit
	if (!_hasGoal)
	{
		_targetY = _playerPositionY + (5 * PIXELS_IN_NODES);//470			// 5 nodes below
		_targetX = _playerPositionX + (2 * _facing * PIXELS_IN_NODES);//300	// 2 nodes ahead

		if (GetPathCount() > 15)
		{
			_pathCount = 0;
			CalculatePathFromXYtoXY(_playerPositionX, _playerPositionY, _targetX, _targetY, PIXEL_COORDS);
		}

		_pathCount++;

		// Search for the exit
		for (int nodeY = 0; nodeY < Y_NODES; nodeY += 1)
		{
			for (int nodeX = 0; nodeX < X_NODES; nodeX += 1)
			{
				if (GetNodeState(nodeX, nodeY, NODE_COORDS) == spExit)
				{
					_hasGoal = true;
					_targetX = nodeX * PIXELS_IN_NODES;
					_targetY = nodeY * PIXELS_IN_NODES;
					CalculatePathFromXYtoXY(_playerPositionX, _playerPositionY, _targetX, _targetY, PIXEL_COORDS);
					return;
				}
			}
		}
	}
	else
	{
		if (GetPathCount() > 60)
		{
			_pathCount = 0;
			CalculatePathFromXYtoXY(_playerPositionX, _playerPositionY, _targetX, _targetY, PIXEL_COORDS);
		}
		_pathCount++;
	}

	//If there is a spike trap nearby
	if (GetNodeState(_playerPositionXNode + (_facing), _playerPositionYNode + 1, NODE_COORDS) == spSpike ||
		GetNodeState(_playerPositionXNode + (_facing * 2), _playerPositionYNode + 1, NODE_COORDS) == spSpike ||
		GetNodeState(_playerPositionXNode + (_facing * 2), _playerPositionYNode + 2, NODE_COORDS) == spSpike ||
		GetNodeState(_playerPositionXNode, _playerPositionYNode + 1, NODE_COORDS) == spSpike &&		// Node below check added for when bot is on very edge of current node
		_canJumpForward &&
		!_isAtLadder)
	{
		_jump = true;
	}

	// Deal with overhanging blocks with blocked passage under them
	if (IsNodePassable(_playerPositionXNode + _facing, _playerPositionYNode, NODE_COORDS) &&			// One node ahead, open
		GetNodeStateForward(1) != spExit &&																// AND NOT the exit
		!IsNodePassable(_playerPositionXNode + (_facing * 2), _playerPositionYNode, NODE_COORDS) &&		// Two nodes ahead, closed
		!IsNodePassable(_playerPositionXNode + _facing, _playerPositionYNode - 1, NODE_COORDS) &&		// One node ahead and up, closed
		_canJumpGrabForward &&
		!_isAtLadder)
	{
		std::cout << "Special case: JUMP OUT" << std::endl;
		_jump = true;
	}

	/**
	 * Special case to jump over gaps that if fallen into would land us in an overhanging block situation
	 * like above, except the bot lands too close to the overhanging block to detect it properly in order to jump
	 */
	if ((IsNodePassable(_playerPositionXNode + (_facing * 3), _playerPositionYNode + 2, NODE_COORDS) /*|| IsNodePassable(_playerPositionXNode + (_facing * 2), _playerPositionYNode + 2, NODE_COORDS)*/) &&			// Three/Two nodes ahead and down 2, open
		(GetNodeState(_playerPositionXNode + (_facing * 3), _playerPositionYNode + 2, NODE_COORDS) != spExit || GetNodeState(_playerPositionXNode + (_facing * 2), _playerPositionYNode + 2, NODE_COORDS) != spExit) &&		// AND they're NOT the exit
		(!IsNodePassable(_playerPositionXNode + (_facing * 3), _playerPositionYNode + 3, NODE_COORDS) /*|| !IsNodePassable(_playerPositionXNode + (_facing * 2), _playerPositionYNode + 3, NODE_COORDS)*/) &&		// Three/Two nodes ahead and down 3, closed
		(!IsNodePassable(_playerPositionXNode + (_facing * 2), _playerPositionYNode + 3, NODE_COORDS) /*|| !IsNodePassable(_playerPositionXNode + (_facing * 1), _playerPositionYNode + 3, NODE_COORDS)*/) &&		// Two/One nodes ahead and down 3, closed
		(!IsNodePassable(_playerPositionXNode + (_facing * 4), _playerPositionYNode + 2, NODE_COORDS) /*|| !IsNodePassable(_playerPositionXNode + (_facing * 3), _playerPositionYNode + 2, NODE_COORDS)*/) &&		// Four/Three nodes ahead and down 2, closed
		(!IsNodePassable(_playerPositionXNode + (_facing * 3), _playerPositionYNode + 1, NODE_COORDS) /*|| !IsNodePassable(_playerPositionXNode + (_facing * 2), _playerPositionYNode + 1, NODE_COORDS)*/) &&		// Three/Two nodes ahead and down 1, closed
		_canJumpGrabForward &&
		!_isAtLadder)
	{
		std::cout << "Special case: JUMP OVER" << std::endl;
		_jump = true;
	}

	//If at exit, use it
	if (GetNodeStateCurrent() == spExit)
	{
		_lookUp = true;
		_levelsCompleted++;
	}
	//You're hanging from a ledge
	else if (_isHanging)
	{
		//If bot wants to go down && there's a drop below
		if ((GetNextPathYPos(_playerPositionXNode, _playerPositionYNode, NODE_COORDS) >= _playerPositionYNode) &&
			(IsNodePassable(_playerPositionXNode, _playerPositionYNode + 1, NODE_COORDS)) &&
			(IsNodePassable(_playerPositionXNode, _playerPositionYNode + 2, NODE_COORDS)))
		{
			_jump = true;
			_duck = true;
		}
		//// If bot wants to go down and there's one space of a drop below
		//else if ((GetNextPathYPos(_playerPositionXNode, _playerPositionYNode, NODE_COORDS) >= _playerPositionYNode) &&
		//	IsNodePassable(_playerPositionXNode, _playerPositionYNode + 1, NODE_COORDS))
		//{
		//	//_jump = true;
		//	_duck = true;
		//}
		else
		{
			_jump = true;

			if (_headingLeft && _canJumpLeft)			// Wants to go left and can jump left
			{
				_goLeft = true;
			}
			else if (_headingRight && _canJumpRight)	// Wants to go right and can jump right
			{
				_goRight = true;
			}
			else										// If we can't go the direction we want
			{
				//TurnAround();		// Taken out for the moment while doing pixel precise checking
			}
		}
	}
	//If you find a ladder   
	else if (_isAtLadder)
	{
		// Set these first so we can change which way to jump off the ladder later if needed
		if (_headingLeft)
		{
			_goLeft = true;
		}
		else
		{
			_goRight = true;
		}

		// if bot wants to go up || can't go down and can't go forward || already going up ladder but can't jump off in either direction
		if (_wantToGoUp && !_goingDownLadder || (!_canFall && !_canGoForward) || (_goingUpLadder && !_canGoLeft && !_canGoRight))
		{
			std::cout << "Going UP ladder." << std::endl;
			_lookUp = true;
			_goingUpLadder = true;
			_goingDownLadder = false;
		}
		else if (_wantToGoDown && !_goingUpLadder || (_goingDownLadder && !_canGoLeft && !_canGoRight))
		{
			std::cout << "Going DOWN ladder." << std::endl;
			_duck = true;
			_goingDownLadder = true;
			_goingUpLadder = false;
		}
		else
		{
			std::cout << "Jumping OFF ladder." << std::endl;

			// If we can't jump off the ladder in the direction we are going, try going the other direction
			if (!_canGoForward && !_canJumpForward)// && !_canJumpGrabForward)
			{
				TurnAround();
			}

			if (_canJumpForward || _canJumpGrabForward)
			{
				_jump = true;
			}

			_goingUpLadder = false;
			_goingDownLadder = false;
		}

		// Reached the top of the ladder
		if (GetNodeStateUp(1) != spLadder)
		{
			std::cout << "Reached TOP of ladder." << std::endl;

			// If we can't jump off the ladder in the direction we are going, try going the other direction
			if (!_canGoForward && !_canJumpForward && !_canJumpGrabForward)
			{
				TurnAround();
			}

			_duck = false;
			_jump = true;

			_goingUpLadder = false;
			_goingDownLadder = false;
		}
	}
	// If going to the right and can continue going that direction
	else if (_headingRight && (_canGoRight || _canJumpRight || _canJumpGrabRight || _onEdgeBeforeWall))
	{
		if (!_canGoRight && !_canFall)
		{
			_jump = true;
		}

		_goRight = true;
		_headingRight = true;
		_headingLeft = false;
	}
	// If going to the left and can continue going that direction
	else if (_headingLeft && (_canGoLeft || _canJumpLeft || _canJumpGrabLeft || _onEdgeBeforeWall))
	{
		if (!_canGoLeft && !_canFall)
		{
			_jump = true;
		}

		_goLeft = true;
		_headingLeft = true;
		_headingRight = false;
	}
	// If going right and cannot continue going that direction - Don't want to chage direction in mid air so added _spIsInAir check
	else if (_headingRight && (!_canGoRight && !_canJumpRight && !_canJumpGrabRight) && !_spIsInAir)
	{
		_goLeft = true;
		_headingLeft = true;
		_headingRight = false;
	}
	// If going left and cannot continue going that direction
	else if (!_spIsInAir)
	{
		_goRight = true;
		_headingRight = true;
		_headingLeft = false;
	}

	//Deal with enemies

	_attack = IsEnemyInNode(_playerPositionXNode + _facing, _playerPositionYNode, NODE_COORDS) ||
		IsEnemyInNode(_playerPositionXNode + (_facing * 2), _playerPositionYNode, NODE_COORDS) ||
		GetIDOfEnemyInNode(spBones, _playerPositionXNode + _facing, _playerPositionYNode, NODE_COORDS) != 0 ||		// Skeleton case
		GetIDOfEnemyInNode(spBones, _playerPositionXNode + (_facing *2), _playerPositionYNode, NODE_COORDS) != 0;

	//Not convinced it's a good idea to stop here - Dan
	/*
	if (_attack)
	{
		_goLeft = false;
		_goRight = false;
	}
	*/

	PrintPlayerVariables();
}

void JordanBot::PrintPlayerVariables()
{
	std::cout << "------------------------------------------" << std::endl;
	std::cout << "Player Variables" << std::endl;
	std::cout << "------------------------------------------" << std::endl;
	std::cout << "spIsInAir: " << _spIsInAir << std::endl;
	std::cout << "isHanging: " << _isHanging << std::endl;
	std::cout << "isAtLadder: " << _isAtLadder << std::endl;
	std::cout << "headingRight: " << _headingRight << std::endl;
	std::cout << "canGoForward: " << _canGoForward << std::endl;
	std::cout << "isAtExit: " << (GetNodeState(_playerPositionXNode, _playerPositionYNode, NODE_COORDS) == spExit) << std::endl;
	std::cout << "targetX: " << _targetX << std::endl;
	std::cout << "targetY: " << _targetY << std::endl;
	std::cout << "------------------------------------------" << std::endl << std::endl;
}

JordanBot::JordanBot() {
	std::cout << "Constructor Called";
	_levelsCompleted = 0;

	performanceInput.open("PerformanceData.txt");
	//TODO - Read data to determine optimal actions
}

void JordanBot::UpdateBools()
{
	// Set this first so _facing can be used correctly in checks setting bools
	if (_headingRight)
	{
		_facing = 1;
	}
	else
	{
		_facing = -1;
	}

	/**
	* Added due to bot thinking he can't go left or right when close enough to detect the wall in front
	* of him but not close enough to fall down the gap at the wall so he used to turn around before falling.
	*/
	_canFall = IsNodePassable(_playerPositionX - (_facing * PIXELS_IN_NODES/4), _playerPositionY + PIXELS_IN_NODES, PIXEL_COORDS);		// Node below is clear (done in pixels to prevent issues where he is on the edge)

	_onEdgeBeforeWall = IsNodePassable(_playerPositionXNode - _facing, _playerPositionYNode, NODE_COORDS) &&		// Node behind is clear
		!IsNodePassable(_playerPositionXNode - _facing, _playerPositionYNode + 1, NODE_COORDS) &&					// Node below and behind is block
		_canFall;																									// Node below registers as clear when on edge

	// NOTE: Need to check for spikes seperately
	_canGoRight = IsNodePassable(_playerPositionX + PIXELS_IN_NODES/2.75, _playerPositionY, PIXEL_COORDS) ||
		GetNodeState(_playerPositionX + PIXELS_IN_NODES / 2.75, _playerPositionY, PIXEL_COORDS) == spSpike;

	_canGoLeft = IsNodePassable(_playerPositionX - PIXELS_IN_NODES/2.75, _playerPositionY, PIXEL_COORDS) ||
		GetNodeState(_playerPositionX - PIXELS_IN_NODES / 2.75, _playerPositionY, PIXEL_COORDS) == spSpike;

	_canGoForward = IsNodePassable(_playerPositionX + (_facing * PIXELS_IN_NODES/2.75), _playerPositionY, PIXEL_COORDS) ||
		GetNodeState(_playerPositionX + (_facing * PIXELS_IN_NODES / 2.75), _playerPositionY, PIXEL_COORDS) == spSpike;

	_canJumpRight = IsNodePassable(_playerPositionXNode + 1, _playerPositionYNode - 1, NODE_COORDS) && IsNodePassable(_playerPositionXNode, _playerPositionYNode - 1, NODE_COORDS);
	_canJumpLeft = IsNodePassable(_playerPositionXNode - 1, _playerPositionYNode - 1, NODE_COORDS) && IsNodePassable(_playerPositionXNode, _playerPositionYNode - 1, NODE_COORDS);
	_canJumpForward = IsNodePassable(_playerPositionXNode + _facing, _playerPositionYNode - 1, NODE_COORDS) && IsNodePassable(_playerPositionXNode, _playerPositionYNode - 1, NODE_COORDS);

	_canJumpGrabRight = IsNodePassable(_playerPositionXNode + 1, _playerPositionYNode - 2, NODE_COORDS) && IsNodePassable(_playerPositionXNode, _playerPositionYNode - 1, NODE_COORDS);
	_canJumpGrabLeft = IsNodePassable(_playerPositionXNode - 1, _playerPositionYNode - 2, NODE_COORDS) && IsNodePassable(_playerPositionXNode, _playerPositionYNode - 1, NODE_COORDS);
	_canJumpGrabForward = IsNodePassable(_playerPositionXNode + _facing, _playerPositionYNode - 2, NODE_COORDS) && IsNodePassable(_playerPositionXNode, _playerPositionYNode - 1, NODE_COORDS);

	_isAtLadder = GetNodeStateCurrent() == spLadder;

	_isHanging = IsNodePassable(_playerPositionX - (_facing * PIXELS_IN_NODES/4), _playerPositionY + PIXELS_IN_NODES, PIXEL_COORDS) &&		// Node below is clear (done in pixels to prevent issues where he is on the edge)
		!IsNodePassable(_playerPositionXNode + _facing, _playerPositionYNode, NODE_COORDS) &&		// Node ahead is block
		IsNodePassable(_playerPositionX + (_facing * PIXELS_IN_NODES/4), _playerPositionY - PIXELS_IN_NODES, PIXEL_COORDS) &&	// Node ahead and up 1 is clear
		!_isAtLadder;

	// Trying out this to see if we can stop bot from changing direction in mid air
	_spIsInAir = IsNodePassable(_playerPositionX - (_facing * PIXELS_IN_NODES/4), _playerPositionY + PIXELS_IN_NODES, PIXEL_COORDS) &&
		!_isHanging &&
		!_isAtLadder;

	_wantToGoUp = GetNextPathYPos(_playerPositionXNode, _playerPositionYNode, NODE_COORDS) < _playerPositionYNode - 1;	// Comparing with node above bot as target is always the top left corner of the node
	_wantToGoDown = GetNextPathYPos(_playerPositionXNode, _playerPositionYNode, NODE_COORDS) > _playerPositionYNode;
	_wantToGoLeft = GetNextPathXPos(_playerPositionXNode, _playerPositionYNode, NODE_COORDS) < _playerPositionXNode;
	_wantToGoRight = GetNextPathXPos(_playerPositionXNode, _playerPositionYNode, NODE_COORDS) > _playerPositionXNode;
}

void JordanBot::TurnAround()
{
	std::cout << "TurnAround() Called" << std::endl;
	_goLeft = !_goLeft;
	_goRight = !_goRight;
	_headingLeft = !_headingLeft;
	_headingRight = !_headingRight;
}

void JordanBot::WriteOutData()
{
	//TODO - Store more meaningful data (expected rewards etc.)
	performanceOutput.open("PerformanceData.txt");
	performanceOutput << "Depth: "				<< _playerPositionY		<< std::endl;
	performanceOutput << "Levels Completed: "	<< _levelsCompleted		<< std::endl;
	performanceOutput.close();
}

double JordanBot::GetNodeStateCurrent()
{
	return GetNodeState(_playerPositionXNode, _playerPositionYNode, NODE_COORDS);
}

double JordanBot::GetNodeStateUp(int num)
{
	return GetNodeState(_playerPositionXNode, _playerPositionYNode - num, NODE_COORDS);
}

double JordanBot::GetNodeStateDown(int num)
{
	return GetNodeState(_playerPositionXNode, _playerPositionYNode + num, NODE_COORDS);
}

double JordanBot::GetNodeStateLeft(int num)
{
	return GetNodeState(_playerPositionXNode - num, _playerPositionYNode, NODE_COORDS);
}

double JordanBot::GetNodeStateRight(int num)
{
	return GetNodeState(_playerPositionXNode + num, _playerPositionYNode, NODE_COORDS);
}

double JordanBot::GetNodeStateForward(int num)
{
	return GetNodeState(_playerPositionXNode + (num * _facing), _playerPositionYNode, NODE_COORDS);
}
