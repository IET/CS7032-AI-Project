This project implements two bots using the Spelunkbots API, JordanBot and LearnBot. The easiest way to run this project is to clone the [Spelunkbots API](https://github.com/GET-TUDA-CHOPPA/SpelunkBots)

```
git clone https://github.com/GET-TUDA-CHOPPA/SpelunkBots.git
```

and run the included copy of GameMaker 8. Then clone this repo and, in GameMaker go to File->open and select the Spelunkbots.gmk file from the spelunky_1_1 directory in this repo.

The selected bot can be changed in Gamemaker by going to `Scripts/AIToolset/BOTSCRIPTS/PlayerChoice` and changing the integer passed to the BotDLLSelector function (JordanBot = 2, LearnBot = 9)

The relevant source code can be found in the `DLL Solution/Bot/SpelunkBot` directory.

## Videos
[Spelunkbot Successfully Completing a Level](https://youtu.be/_NW6Q_C_t_4)

[Death by Trap: Showing the Shortsightedness of the Bot in Terms of Trap Avoidance](https://youtu.be/Vq8hnSLUXqk)

[Pathfinding1: Showing the Limitations of Spelunkbots' Built-in Pathfinding](https://youtu.be/rw306HkcOCU)

[Pathfinding2: Showing the Difficulty of Finding a Path Through Certain Game Geometry](https://youtu.be/6GU64LA4z-8)
