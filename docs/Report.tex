\documentclass{sig-alternate-05-2015} % V1.2


\doi{10.1145/1559755.1559763}

%ISBN
\isbn{123-4567-24-567/08/06}

% Copyright
\setcopyright{acmcopyright}

\begin{document}

% Page heads
%\markboth{Daniel Walsh \& Evan White}{CS7032 - Artificial Intelligence Project Report}

% Title portion
\title{CS7032 - Artificial Intelligence Project Report}

\numberofauthors{2}
\author{
	\alignauthor
		Daniel Walsh\\
	 	\affaddr{Trinity College Dublin}\\
		\email{walshd15@tcd.ie}
	\alignauthor 
		Evan White \\
		\affaddr{Trinity College Dublin}\\
		\email{whiteev@tcd.ie}
}

\maketitle

\begin{abstract}
%%TODO - Write something about what we did.
In this report, we describe our attempt to create an agent capable of playing the popular video game Spelunky, using the Spelunkbots API.

Spelunky is a highly challenging game requiring a player to explore a series of procedurally generated levels filled with various traps and enemies, collecting treasures and searching for the exit.

%% I had to try very hard to not make this sound like a preview of an episode of a TV show
The game can be played in a number of different ways but always requires a considerable amount of planning and skill to complete and so represents a considerable challenge for AI agents to play effectively.

\end{abstract}

\section{Introduction}
%%Problem Description

\subsection{Spelunky AI Competition} \label{sec:spelunkyAIcomp}
For this project, we decided to use the Spelunkbots C++ API to develop a bot to play the game Spelunky. Spelunky is a game in which a player must navigate caves full of traps and dangerous enemies collecting treasure along the way. In the game each level is randomly generated so a player must be able to adapt to the different situations that may present themselves but in the interest of a fair comparison, the proposed Spelunkbots competition has three separate modes of evaluating each bot:

\begin{enumerate}
	\item{\textbf{Public Training Set:} A series of levels made available to the public in advance of the competition.}
	\item{\textbf{Private Training Set:} A hidden set of levels that will not be publicly available but revealed with the final competition results.}
	\item{\textbf{Marathon:} In which the bot is given a number of attempts to play the full Spelunky game from start to finish.}
\end{enumerate}

The bots are assessed in terms of either score (amount of treasure collected), or in terms of how quickly they complete levels. For this project we decided to concentrate on time and priortise effective navigation and survival over collection of treasures.

Strangely many of the public training set levels require the use of items, while the marathon mode in the game generally does not require any items but they can provide a great advantage to the player. For this reason, our target was the marathon mode rather than the public training set, concentrating on the problems of map traversal, trap avoidance, and combat with enemies.

\subsection{Problem Description}

In the game Spelunky, players are required to navigate randomly generated maps populated with traps and monsters. The game is known for its unforgiving nature, very little exists in Spelunky that is not trying to kill the player in some way.

As a player progresses through the game, they must navigate multiple different worlds starting with a cave, then jungle, ice, and a final, more difficult cave-world. With the exception of the ice world, the mechanics are common among all of the worlds, with the player starting at the top level of the map and navigating down to an exit at the bottom level. In the ice world, the player navigates floating platforms attempting to find the exit without falling to their death. Because of this inconsistency, the ice world is unpopular with fans and poses very different challenges for the development of an AI bot. For this reason, we are targeting only the cave levels with our bot, with possible extension to the jungle later, adding functionality to deal with more traps and enemies.

More details on the Spelunky game can be found in the paper ``With Fate Guiding My Every Move: The Challenge of Spelunky'' \cite{Thompson2015}, in which Tommy Thompson, one of the authors of the Spelunkbots API describes the challenges of Spelunky for a player agent and proposes the game as a benchmark problem for AI, similar to John McCarty's paper on the Lemmings game\cite{McCarty1998}.

\subsubsection{Percepts} \label{sec:percepts}

When describing the probem faced by an artificially intelligent agent, and indeed the problem faced by a developer when creating an artificially intelligent agent, we must look at the information that is available to such an agent, or what is \emph{perceptible}. The game world is broken up into nodes, each of which is 16x16 pixels in size. These nodes can have several different states which are used to determine what is contained within the node. The main node types are terrain, collectable and threat. Some examples of these states and types, and how they are represented in the Spelunkbots API can be seen in Table~\ref{table:node_states}.

\begin{table}[htbp]
\centering
	\begin{tabular}{|c|c|c|}
		\hline \bf Terrain & \bf Collectable & \bf Threat \\ \hline
		spEmptyNode & spGoldBar & spBat \\ \hline
		spStandardTerrain & spDiamond & spSpider \\ \hline
		spLadder & spChest & spGiantSpiderHanging \\ \hline
		spExit & spJetpack & spGiantSpider \\ \hline
		spEntrance & spBomb & spBones \\ \hline
		spSpike & spRock & spCaveman \\ \hline
		& spShotgun & spSnake \\ \hline
		& spFlare & spSmashTrap \\ \hline
		& spGoldIdol & spCeilingTrap \\ \hline
		& spLampItem & spBoulder \\ \hline
	\end{tabular}
 \caption{Different Terrain Types Defined in Spelunkbots API~\cite{scales2014create}}
 \label{table:node_states}
\end{table}

\subsubsection{Actions}
In any typial implementation of an artificially intelligent agent, the agent must be able to interact with the environment through a set of \emph{actions}. In the case of the Spelunkbots API, and in many APIs which are designed for implementing an agent that plays a game, the set of actions that are available to us essentially control which buttons are being ``pressed" in each frame. This is quite an intuitive way to think about the game and how the agent will interact with it as it mimics exactly how a human player would interact with it. The actions, or control variables, available to us can be seen in Table~\ref{table:actions}.

%%TODO - Confirm which of these actions are available in C++ and GML
\begin{table}[htbp]
\centering
	\begin{tabular}{|c|c|}
		\hline \bf Control Variable & \bf Action on Next Frame\\ \hline
		goLeft & Move Left \\ \hline
		goRight & Move Right \\ \hline
		jump & Attempt to Jump \\ \hline
		duck & Duck/Descend Ladder \\ \hline
		lookUp & Look Up/Ascend Ladder \\ \hline
		attack & Attack with Item (e.g. Whip) \\ \hline
		running & Run (GameMaker only) \\ \hline
		payp & Pay for Item in Shop (GameMaker only) \\ \hline
		ropep & Release a Rope (Incomplete in C++) \\ \hline
		bomp & Release a Bomb (GameMaker only) \\ \hline
	\end{tabular}%}
 \caption{Control Variables Avaiable to Control the Agent in Spelunkbots API~\cite{scales2014spelunkbots}}
 \label{table:actions}
\end{table}

\subsubsection{Goals}
While there are many goals in Spelunky, (e.g. collecting treasure, rescuing the damsel, defeating enemies, not being hurt by enemies/traps, getting to the exit of a level as quickly as possible) we decided to narrow our range of goals in order to focus on the behaviour and intelligence of an agent in a specific area. The goals for our agents is to navigate to the exit of the level as quickly as possible and, in the case of the reactive agent, to reduce damage taken by defending himself from nearby enemies.

\subsubsection{Environment}
As described in Section~\ref{sec:percepts}, the game world is divided up into nodes, and it is through performing checks on the status of these nodes that the agent can get a sense of its \emph{environment}. The Spelunky game world environment has an interesting set of characteristics which must be taken into account when designing an artificially intelligent agent capable of navigating and surviving it:

\begin{itemize}
	\item{The environment is \emph{partially observable}. An agent, or player, can only see a limited section of the level at any one time, and not the enitre level istelf. The visible area of the map is limited to 41 nodes along the x axis and 33 nodes along the y axis, giving a total of 346,368 pixels in the total visible area. This means that the agent cannot obtain certain information about the level it is currently in, such as the location of the exit, the location of any enemies or threats off screen, whether continuing in a certain direction will lead to a dead end or not and what direction to go in will ultimately be the best.}
	\item{The environment is \emph{stochastic} or \emph{non-deterministic}. The next state of the environment is not fully determined by the current state and action performed by the agent. The enemies in the environment act on their own will and can freely move around the level. Generally, if an enemy detects the agent then they will attempt to move towards it to inflict damage, however, depending on the enemy, if they can not currently detect the agent, they wander around the level independently of the agent's actions.}
	\item{In the case of our reactive agent, the environment can be described as \emph{episodic}, as the reactive agent's next action depends solely on the current state of the environment; that is, the current state of the environment as far as the agent is able to perceive. However, in the case of our learning agent, the environment can be described as \emph{sequential}. The learning agent takes its next action based on the current state of the environment and its perceived reward for taking a particular action in that particular state, if it has encountered that state previously.}
	\item{The environment is \emph{dynamic}. Moving enemies can change the state of the environment independently of the agent's actions. There is also a timer that is continuously ticking, keeping track of the length of time it has taken the agent to complete the level.}
	\item{It could be argued that the environment is \emph{discrete}, due to the limited space that is visible at any one time to the agent, the limited and fixed set of actions which the agent is capable of performing, the limited number of states each node in the game world can have, and the agent's limited perceptual abilities when it comes to checking the state of these nodes. However, it could also be argued that the environment is \emph{continuous}, due to the fact that levels may be proceedurally generated and so may appear in an extremely large number of variations, giving an extremely large number of possible states and so possible percepts on the environment depending on the configuration of the level.}
	\item{There can be only one agent running at any one time and so this is a \emph{single-agent} environment. This adds an interesting constraint when designing an agent for Spelunky. If it were possible to have multiple agents in the environment then this may open up the potential for communication between agents, allowing each agent to go in a distinct direction and possibly let the other agent know if it had found a dead end or a succesful path to the exit that may have been outside of the field of view of the other agent. As this is not the case, the agent must fend for themselves using their limited view and knowledge of each level at each moment.}
\end{itemize}

\section{Approach Taken}
%%TODO - Mention how litte care he takes while wandering the level and how often he dies.

\subsection{Reactive Spelunkbot} \label{sec:reactive_bot_approach}
Our initial approach was to create a reactive bot that would simply move through the level and respond to the status of the nodes adjacent to their current location. The advantages of this approach are that it is the most intuitive way to begin programming a bot and that it will give us an intuition as to the important state variables needed to later attempt a dynamic programming approach. The crucial disadvantage of this method however, is that we must explicitly define each state that our agent will need to respond to, creating a list of states that will never be exhaustive, especially if it is to be able to complete an arbitrary, randomly-generated Spelunky map.

Despite the procedurally generated nature of the levels in a standard game of Spelunky, the reactive agent we developed is quite successful at navigating to the exit in many of the situations that arise in the marathon mode mentioned in Section~\ref{sec:spelunkyAIcomp}. Creating a reactive agent was extremely time consuming as it involved the process of programming, running, observing, re-programming, re-running, observing, etc. over and over again. Although tedious, this process produced a reasonably satisfactory result, even if the resulting agent may not be considered all that ``intelligent". The reason for the success of this approach, and the only way to make developing a reactive agent reasonably more feasible, was by identifying the most commonly occurring situations in which the agent would have difficulty, and then catering for that situation specifically by creating a $state \rightarrow action$ pair. While it would be impossible to create a $state \rightarrow action$ mapping for every possible situation in which the agent may find itself, by limiting the state space to the most commonly occurring states we found that a reactive agent navigated quite well.

As the main goal of our agent was to reach the exit, it was obvious that the highest layer of our subsumption hierarchy (i.e. the state with highest priority) should be if the goal, i.e. exit, is within sight of the agent. This is catered for by creating a special condition wherein if the agent could see the exit then its target in world space was set to the location of the exit and the path to it was recalculated (using the Spelunkbots provided $A*$ pathfinding algorithm) on a regular basis to ensure that the agent navigates towards it. Once the path has been recalculated, the agent follows its usual rules governing movement to assist in actually getting the agent to the exit.

A number of special states are checked for at the beginning of each update cycle to ensure they are correctly addressed. These involve checking for nearby spike traps and jumping over them, as well as jumping over or out of a particularly geometrical layout where the agent was prone to get stuck and thus inhibited further progression. This particular geometrical layout can be seen in Figure~\ref{fig:overhanging_block}. After these special states are checked and addressed if necessary, the subsumption hierarchy proceeds to check the state as follows:

\begin{enumerate}
	\item{If the agent is currently at the exit.}
	\item{If the agent is hanging off the side of a block.}
	\item{If the agent is at a ladder.}
	\item{If the agent is going right and can continue to go right.}
	\item{If the agent is going left and can continue to go left.}
	\item{If the agent is going right and cannot continue to go right.}
	\item{Finally, a fall back case if all others evaluate to false, and so the default behaviour of the agent, which is essentially to go right.}
\end{enumerate}

Within these checks there are some additional checks to cater for more specific states after having identified the general state, for example if the agent is at a ladder, it then checks to see if the next node on its path is above or below it and goes up or down the ladder accordingly. The agent is also constantly checking for enemies in the nearby nodes and if it detects one then it calls the attack action. This is checked and happens regardless of any other state.

\begin{figure}[h]
\centerline{\includegraphics[width=8.5cm]{overhanging_block}}
\caption{The ``overhanging block" structure (circled) that would result in an agent getting stuck that is catered for by one of the high priority checks at the start of each update cycle.}
  \label{fig:overhanging_block}
\end{figure}

%%TODO - Expand this to include details of how the bot handles different scenarios
%%Like the way attack is triggered when an enemy is within two nodes of the player.

\subsection{Learning Spelunkbot}
%%Not really sure about tone of this section. Maybe should read more like a proper paper? 
Once we were more familiar with the Spelunkbots API, we decided to attempt to make a learning agent to play the game. Spelunky seemed like a natural fit for a learning agent because of the issue of defining each and every situation that an agent may find themselves in with a game using randomly generated levels.

A major issue with implementing a learning algorithm was to define the different states of the environment, as it would quickly become unmanageable if you were to encode everything visible on screen as part of the state. For this reason, our learning agent operates on a very simplified model where it is only aware of the nodes immediately adjacent to it. We then further simplified this model by reducing the valid values of each node from the eleven exposed by the API (more if you include different enemy types) to only a binary value of whether or not a node is passable. Ignoring the value of the node where the player is currently located means that state can be encoded in eight bits representing the eight nodes surrounding the player, as shown in Figure~\ref{fig:player_state}. 

Further simplifications could be made since some of these states are impossible (such as being completely surrounded by impassible blocks) or not necessarily distinct (does the height of neighbouring geometry matter as long as it can be jumped over?) but for ease of implementation further optimisations were ignored, leaving us with a fairly manageable 256 possible states.

\begin{figure}[h]
\centerline{\includegraphics[width=5cm]{player_state}}
\caption{The area around the player used to determine their state.}
  \label{fig:player_state}
\end{figure}

The set of available actions was also reduced to further simplify the model. In a normal game a player may move left or right, jump, attack, look up or down, throw ropes or bombs, pick up and throw items, and even purchase items in a shop if one is found. Our approach was to start as simple as possible and use only four simple movement actions: move left, move right, jump left, and jump right. This meant that our learning bot would not be able to play the marathon mode of the game so a test level was created that needed only basic movement so that we could test the bot without having it die repeatedly because of the lack of hazards in its view of the environment despite their abundance in the game world. The basic layout of the test level is shown in Figure~\ref{fig:test_lvl}

\begin{figure*}[t]
\centerline{\includegraphics[width=18cm]{leaning_test_lvl}}
\caption{The Test Level Created for the Learning Bot. Left of the Entrance is a Hole that a Player Needs to Drop Through to Reach the Exit Below}
  \label{fig:test_lvl}
\end{figure*}

The next issue was the choice of learning algorithm, for this Temporal Difference Learning seemed the obvious choice since it can learn directly from raw experience without a model of the environment's dynamics, which is necessary because our method of encoding state does not guarantee that one state will always transition to the same state given a particular action and so a more statistical method is required.

The Sarsa algorithm \cite{Rummery1994} seemed simple enough to implement in the time available and offered two major advantages over similar approaches. Firstly, it uses on-line learning, allowing value estimates for state/action pairs to be updated without a final outcome, meaning that we can define a level as one episode but allow the bot to receive feedback without first completing the level (which, if using an initial random policy would require a lot of time and luck). The second major advantage to Sarsa is that it implements a greed factor $\epsilon$, allowing use to balance exploitation and exploration in the bot's policy for selecting actions. Defining a threshold for exploration seems intuitively useful in Spelunky since much of the game depends on exploration and learning from the current level so a fully greedy approach would be unlikely to be very useful.

Estimated values for different state/action pairs are written out to a file when the bot completes a level so that subsequent iterations will have some initial learning data. This does mean that if the bot is unsuccessful, all that it has learned will be discarded but it is likely that the discarded learning data would be of a lower quality so there should be little cost to discarding it.

\section{Results}

\subsection{Reactive Spelunkbot}
Due to the extremely high number of possible states that can occur, our reactive agent can still run into difficulty in certain situations. Given more time, it would be possible to extend the reactive agent's abilities allowing it to react to a larger range of states. However, this would most likely be extremely time consuming and tedious, involving multiple iterations of the program, run, observe cycle mentioned in Section~\ref{sec:reactive_bot_approach}. By limiting ourselves to focussing on goals of reaching the exit and defending against enemies, and limiting ourselves to the most commonly occurring states, and states which caused the most difficulty for the agent, we achieved an agent that is reasonably efficient at achieving its goals.

In most cases the reactive agent successfully navigates to the exit. However, as it is not concerned with its own health with regards to certain types of traps and falling great distances, the agent can take a lot of damage from threats other than enemies. This resulted in the agent rarely making it past 2 levels of the marathon mode successively due to the fact that health is not replenished at the end of each level.

The reactive agent performed very well on the first few levels in the public test set, however not all levels in the public test set could be successfully navigated due to items such as ropes and bombs not being implemented in the C++ API, which were required to complete some of the public test set levels.

Overall the reactive agent we developed is highly successful at navigating to the exit in most situations and only has issues with a small set of unusually occurring situations. Navigation on ladders could be improved as the agent, while in most cases successfully handles climbing and getting off of a ladder, can sometimes become stuck in a loop of jumping off of the ladder, going back to it and climbing the ladder again. To improve the navigation on ladders, however, would involve much more research into the Spelunkbot API's provided $A*$ pathfinding algorithm as this seems to be the root cause of the agent getting stuck in a loop of climbing and dismounting a ladder due to the next point on the agent's path being determined by the $A*$ algorithm as being above the agent, and then once on the ladder the nearest point on the path is determined as below the agent. Given more time, and a greater understanding of pathfinding algorithms, our reactive agent could be improved to navigate ladders better than it currently does, although it navigates them to an acceptable standard with our implementation.

\subsection{Learning Spelunkbot}
\label{sec:learn_bot}

Unfortunately due to time constraints, we were unable to expand on the abilities of the learning spelunkbot at all and so it had some major weaknesses, mostly arising from the simplicity of its model.

For instance, given the two screenshots in Figure ~\ref{fig:test_lvl}, in each case the bot will perceive the same state yet in the second one at the lower level, the exit is visible to the right so clearly actions taking the player in that direction should be valued more highly but the bot does not realise this so it depends on exploration to randomly guide it towards the exit. A possible fix would be to assign an additional reward for getting closer to the exit (once it is visible), so that the bot can more completely evaluate the quality of actions.

Impressively, despite its simplicity the bot did perform substantially better than random chance and could effectively update its value estimates in real time with no decreased performance of the game evident. It is unclear how well this approach would scale however, since the actual game has more than 10 valid states for each node. Also, when played by a human, the game often requires a considerable amount of planning and foresight in order to complete a level, including a limited number of backups would help the bot be less myopic but this would introduce further scalability issues so it is unlikely to be viable.

Because of the enormity of the list of possible states, it is unclear how viable temporal difference learning is for solving this problem but further testing would be required to confirm this. %Yeah, it's the algorithm's fault...

\section{Difficulties}
\subsection{Spelunkbots API}
%%TODO - Less flowery language - Dan
The Spelunkbots competition was chosen because it seemed like an interesting challenge based on an enjoyable game but unfortunately significant difficulty was added by the API itself, often causing difficult to diagnose bugs.

The documentation of the API \cite{SpelunkbotsDocs} is largely out of date or incomplete and following the online guide will allow you to load a game but most of the included bots will not move at all and none have very effective movement, meaning there was considerable effort involved in developing even a very simple bot. This meant that there was many hours of work needed to familiarise ourselves with the API and to try to perform even simple movement. To make matters worse, the project appears to be completely dead at the moment so issues added to their Google Groups page \cite{SpelunkbotsGG} would go completely unanswered. We ultimately decided that to change to a different competition would be more costly than continuing with Spelunkbots.

Later in the project, we had to edit the API itself because some of the actions available to a player were not fully implemented in the C++ API. This was an easy issue to fix but it cost us a considerable amount of time trying to identify the issue that ended up being independent of our implementation. This does mean that strictly speaking our bot could not be entered into a competition using the Spelunkbots API but it is our belief that a competition could not reasonably be run with the API in its current state.

When it came to implementing a learning algorithm, the lack of functionality to simply run a level for a certain number of iterations was seen as a major issue but thankfully there is an option to write bots entirely in C++, which allows a lot more freedom than interfacing with the API through GameMaker, meaning that the standard file IO functionality of C++ could allow the bot to save learned data to a file at the end of an episode and re-load it on the next execution. Though not ideal, it at least allowed for somewhat longer-term learning.

\section{Usefulness of AI in Solving This Problem}
Of course, being a video game there's no real use in developing an AI agent to play it for you but it is important to consider the value of the problem as a means to further AI research.

Similar to Lemmings, as described in McCaty's 1998 paper\cite{McCarty1998} Spelunky poses an interesting challenge for AI agents. The two games share many features, such as being only partially observable, requiring a high degree of planning with only partial knowledge of the environment, and a very large set of possible environment states.

Though very similar, there are some major differences between the two. Firstly, though the physics of Spelunky are very tightly controlled such that it can be said to be deterministic, the levels in marathon mode are randomly generated so that unlike in Lemmings, they cannot simply be replayed learning from mistakes with each iteration meaning a more general model is required for playing Spelunky effectively. The second major difference is that in Lemmings, it is impossible to individually control each lemming while in Spelunky, controlling your character is not a part of the challenge.

An interesting quality of Spelunky for an AI agent is that levels may be played in a large variety of different ways, players can prioitise speed or score, bombs may be used to destroy geometry, items may be exploited for a variety of advantages, gold may be used to buy additional items, and the player can even seek out powerful enemies for the useful items that they drop when defeated. This could be especially interesting for machine learning approaches, as different agents could exhibit different ``play-styles'', similar to a human player.

As with Lemmings, a strong argument could be made for the usefulness of Spelunky in testing AI approaches and improving upon current algorithms that may them be extended to other areas\cite{Thompson2015}.

\subsection{Learning Bot}
As mentioned in Section~\ref{sec:learn_bot}, a lot of discussion and planning went into trying to develop a simplified model of the Spelunky environment such that a finite number of states could be defined. This proved to be quite time-consuming and the very limited model finally developed was simply not detailed enough for the bot to make particularly meaningful decisions.

Another issue with the temporal difference implementation was the very simplistic rewards strategy in which the bot receives a reward of +1 for gaining depth in the level (being them closer to the exit) and a reward of +2 for completing a level. The main issue with this reward scheme is that if the bot is stuck in a section of the level, there is no way that it will be rewarded for escaping unless it reaches a greater depth. This proved to be very troublesome in practice and often led to the bot getting stuck in a loop.

Finally, a combination of the above issues is that in the case where the bot can see the exit, it does not favour actions bringing it closer to the exit because this data is not included in the determination of state and no additional value/reward is added based on this parameter.
 
\section{Individual Contributions}

\begin{table}[htbp]
\centering
	\begin{tabular}{|p{3cm}||c|c|}
		\hline \bf 	Task 										& \bf 	Daniel 			& 	\bf Evan 		\\ \hline
					Initial Setup								&		 				& 		x 			\\ \hline
					Reactive Spelunkbot 						& 	  	- 				& 		x			\\ \hline
					Discussion and Planning of Learning Spelunkbot & 	x 				& 		x 			\\ \hline
					Implementation of Learning Spelunkbot 		& 		x 				& 		 			\\ \hline
					Testing and Bug-Fixes						&		-				&		x 			\\ \hline
					Documentation 								&		x				&		x			\\ \hline
					%%Writing This section 						&		x				&					\\ \hline
	\end{tabular}
 \caption{Summary of Contributions by Team Members With ``x'' Denoting a Major Contribution and ``-'' Denoting Some Contribution}
 \label{table:individual_contributions}
\end{table}

A summary of each team member's individual contributions is shown in Table ~\ref{table:individual_contributions}. A more detailed description is given in the following sections.

\subsection{Initial Setup}
After downloading the API and following the ``Getting Started'' guide the project is still not functional and requires changes to a few of the scripts in order to run at all. It was then necessary to test and evaluate each of the example bots in order to determine which one we should build upon in order to implement our reactive spelunkbot (JordanBot was ultimately chosen). This work was almost exclusively carried out by Evan.

\subsection{Reactive Spelunkbot}
The initial work on the reactive spelunkbot was carried out by Daniel. This largely revolved around basic traversal of the level, in particular with the problems of the bot getting stuck hanging on edges and not being able to use ladders. A basic check was also added so that the bot would attack if an enemy was within two tiles in the direction that the bot was facing.
Subsequent work was performed by Evan, adding trap avoidance, more complex traversal and generally optimising the bot to more often reach the exit of any given random level. During this phase a major change was made to how the bot observes its surroundings, using pixel-based checks to more accurately check for traps, enemies, and level geometry blocking the bot.

\subsection{Discussion and Planning of Learning Spelunkbot}
At the same time as work was continuing on the reactive bot, plans were being made for a bot that would implement a machine learning algorithm and given enough time, hopefully prove to be more effective and adaptable than the purely reactive version.
Bot team members contributed to this phase, settling on a temporal difference learning approach. The specific temporal difference algorithm to be used was chosen by Daniel.

\subsection{Implementation of Learning Spelunkbot}
The implementation of the learning spelunkbot was performed by Daniel while Evan was still increasing the success rate of the reactive bot. Early tests were promising but unfortunately the learning bot was only implemented in a fairly limited capacity and never managed to perform comparably to the reactive bot.

\subsection{Testing and Bug-Fixes}
Both team members contributed to this section but most of the larger (usually API related) bugs were caught by Evan.

\subsection{Documentation}
Both team members contributed equally, mostly documenting the areas of the project they were most familiar with as discussed above.


% Bibliography
\bibliographystyle{abbrv}
\bibliography{references}
\balancecolumns
\end{document}
